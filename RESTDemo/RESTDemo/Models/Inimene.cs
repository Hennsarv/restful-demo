﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RESTDemo.Models
{
    public class Inimene
    {
        static Dictionary<int, Inimene> _Inimesed = new Dictionary<int, Inimene>();
        static int nr = 0;

        [Key] public int Id { get; private set; }
        public string Nimi { get; set; }
        public int Vanus { get; set; }

        public static IEnumerable<Inimene> Inimesed => _Inimesed.Values;

        public static Inimene Find(int id)
            => _Inimesed.Keys.Contains(id) ? _Inimesed[id] : null;

        public void Add()
        {
            if (Id == 0)
            {
                Id = ++nr;
                _Inimesed.Add(nr, this);
            }
        }

        public Inimene Edit(Inimene uus)
        {
            this.Nimi = uus.Nimi;
            Vanus = uus.Vanus;
            return this;
        }

        public void Delete()
        {
            if (_Inimesed.Keys.Contains(this.Id)) _Inimesed.Remove(this.Id);
        }


    }
}