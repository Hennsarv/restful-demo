﻿using System; 
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using RESTDemo.Models;

namespace RESTDemo.Controllers
{
    public class HumanController : ApiController
    {
        // GET: api/Human
        public IEnumerable<Inimene> Get()
        {
            return Inimene.Inimesed;
        }

        // GET: api/Human/5
        public Inimene Get(int id)
        {
            return Inimene.Find(id);
        }

        // POST: api/Human
        public void Post([FromBody]Inimene value)
        {
            value.Add();
        }

        // PUT: api/Human/5
        public void Put(int id, [FromBody]Inimene value)
        {
            Inimene.Find(id)?.Edit(value);
        }

        // DELETE: api/Human/5
        public void Delete(int id)
        {
            Inimene.Find(id)?.Delete();
        }
    }
}
