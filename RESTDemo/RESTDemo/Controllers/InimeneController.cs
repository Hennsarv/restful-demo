﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RESTDemo.Models;

namespace RESTDemo.Controllers
{
    public class InimeneController : Controller
    {
        // GET: Inimene
        public ActionResult Index()
        {
            return View(Inimene.Inimesed);
        }

        // GET: Inimene/Details/5
        public ActionResult Details(int id)
        {
            return View(Inimene.Find(id));
        }

        // GET: Inimene/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Inimene/Create
        [HttpPost]
        public ActionResult Create(Inimene inimene)
        {
            inimene.Add();
            return RedirectToAction("Index");
        }

        // GET: Inimene/Edit/5
        public ActionResult Edit(int id)
        {
            return View(Inimene.Find(id));
        }

        // POST: Inimene/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Inimene inimene)
        {
            Inimene.Find(id)?.Edit(inimene);
                return RedirectToAction("Index");
        }

        // GET: Inimene/Delete/5
        public ActionResult Delete(int id)
        {
            return View(Inimene.Find(id));
        }

        // POST: Inimene/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, Inimene inimene)
        {
            Inimene.Find(id)?.Delete();
                return RedirectToAction("Index");
        }
    }
}
