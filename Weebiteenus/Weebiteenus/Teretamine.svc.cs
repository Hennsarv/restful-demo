﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Weebiteenus
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Teretamine" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Teretamine.svc or Teretamine.svc.cs at the Solution Explorer and start debugging.
    public class Teretamine : ITeretamine
    {
        public string Tere(string nimi)
        {
            return "Tere " + nimi + "!";
        }
    }
}
